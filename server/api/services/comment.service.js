import commentRepository from '../../data/repositories/comment.repository';

export const create = (userId, comment) => commentRepository.create({
    ...comment,
    userId
});

export const update = (id, comment) => commentRepository.updateById(id, comment);

export const deleteComment = id => commentRepository.deleteById(id);

export const getCommentById = id => commentRepository.getCommentById(id);

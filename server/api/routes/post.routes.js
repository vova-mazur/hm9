import { Router } from 'express';
import * as postService from '../services/post.service';

const router = Router();

router
    .get('/', (req, res, next) => postService.getPosts(req.query)
        .then(posts => res.send(posts))
        .catch(next))
    .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
        .then(post => res.send(post))
        .catch(next))
    .post('/', (req, res, next) => postService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then((post) => {
            req.io.emit('new_post', post); // notify all users that a new post was created
            return res.send(post);
        })
        .catch(next))
    .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(({ toChange, reaction }) => {
            if (reaction && reaction.post && (reaction.post.userId !== req.user.id)) {
                // notify a user if someone (not himself) liked his post
                if (reaction.dataValues.isLike) {
                    req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
                } else {
                    req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
                }
            }
            return res.send({ toChange, reaction });
        })
        .catch(next))
    .put('/:id', (req, res, next) => postService.update(req.params.id, req.body)
        .then((post) => {
            req.io.emit('update_post', post);
            return res.send(post);
        })
        .catch(next))
    .delete('/:id', (req, res, next) => postService.deletePost(req.params.id)
        .then((post) => {
            req.io.emit('delete_post', post);
            return res.send({ id: req.params.id });
        })
        .catch(next))

export default router;

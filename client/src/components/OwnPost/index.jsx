import React from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'semantic-ui-react';
import Post from '../Post';

const OwnPost = ({ post, likePost, dislikePost, toggleExpandedPost, sharePost, updatePost, deletePost }) => (
  <Card style={{ width: '100%' }}>
    <Post
      post={post}
      likePost={likePost}
      dislikePost={dislikePost}
      toggleExpandedPost={toggleExpandedPost}
      sharePost={sharePost}
    />
    <Card.Content extra>
      <div className="ui two buttons">
        <Button basic color="green" onClick={() => updatePost(post)}>
          Edit
        </Button>
        <Button basic color="red" onClick={() => deletePost(post.id)}>
          Delete
       </Button>
      </div>
    </Card.Content>
  </Card>
);

OwnPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
};

export default OwnPost;
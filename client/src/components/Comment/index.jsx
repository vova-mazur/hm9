import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Form, Button } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({ comment: { id, body, createdAt, user }, updateComment, deleteComment }) => {
    const date = moment(createdAt).fromNow();

    const [commentText, setText] = useState(body);
    const [editMode, setEditMode] = useState(false);

    const renderCommentEdit = () => (
        <Form reply onSubmit={handleEditComment}>
            <Form.TextArea
                value={commentText}
                placeholder="Type a comment..."
                onChange={ev => setText(ev.target.value)}
            />
            <Button type="submit" content="Ok" labelPosition="left" icon="edit" primary />
        </Form>
    );

    const handleEditComment = async event => {
        event.preventDefault();

        if(!commentText) {
            return;
        }
        await updateComment(id, { body: commentText });
        setEditMode(false);
    };

    const deleteHandler = () => {
        if(window.confirm('Do you realy want to delete this comment?')) {
            deleteComment(id);
        }
    }

    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">
                    {user.username}
                </CommentUI.Author>
                <CommentUI.Metadata>
                    {date}
                </CommentUI.Metadata>
                <CommentUI.Actions>
                    <CommentUI.Action>
                        <Button onClick={() => setEditMode(true)}>update</Button>
                    </CommentUI.Action>
                    <CommentUI.Action>
                        <Button onClick={deleteHandler}>Delete</Button>
                    </CommentUI.Action>
                </CommentUI.Actions>
                {editMode ? renderCommentEdit() :
                    <CommentUI.Text>
                        {body}
                    </CommentUI.Text>
                }
            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    updateComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
};

export default Comment;

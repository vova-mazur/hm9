export const SET_CURRENT_POST = "SET_CURRENT_POST";
export const DROP_CURRENT_POST = "DROP_CURRENT_POST";
export const SHOW_PAGE = "SHOW_PAGE";
export const HIDE_PAGE = "HIDE_PAGE";
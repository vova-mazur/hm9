import {
  SET_CURRENT_POST, DROP_CURRENT_POST,
  SHOW_PAGE, HIDE_PAGE
} from './actionTypes';

const initialState = {
  currentPost: {},
  isShown: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_POST: {
      const { post } = action;
      return {
        ...state,
        currentPost: post
      }
    }
    case DROP_CURRENT_POST: {
      return {
        ...state,
        currentPost: {}
      }
    }
    case SHOW_PAGE: {
      return {
        ...state,
        isShown: true
      }
    }
    case HIDE_PAGE: {
      return {
        ...state,
        isShown: false
      }
    }
    default:
      return state;
  }
}
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal, Header, Form, Button, Icon, Image, Segment } from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';
import * as actions from './actions';
import { updatePost } from '../Thread/actions';

import styles from './styles.module.scss';

class PostEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      body: '',
      imageId: undefined,
      imageLink: undefined,
      isUploading: false
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.currentPost) {
      const { body, image } = newProps.currentPost;
      this.setState({
        body,
        imageLink: image ? image.link : undefined
      })
    }
  }

  handleEditPost = async () => {
    const { imageId, body } = this.state;
    if (!body) {
      return;
    }
    const { currentPost } = this.props;
    await this.props.updatePost(currentPost.id, {
      imageId,
      body
    });
    this.props.dropCurrentPost();
    this.props.hidePage();
  }

  onCancel = () => {
    this.props.dropCurrentPost();
    this.props.hidePage();
  }

  uploadImage = file => imageService.uploadImage(file);

  handleUploadFile = async ({ target }) => {
    this.setState({ isUploading: true });
    try {
      const { id: imageId, link: imageLink } = await this.uploadImage(target.files[0]);
      this.setState({ imageId, imageLink, isUploading: false });
    } catch {
      // TODO: show error
      this.setState({ isUploading: false });
    }
  }

  render() {
    const { imageLink, body, isUploading } = this.state;
    return (
      <Modal open={this.props.isShown} style={{ width: '400px' }}>
        <Modal.Header>Edit a Post</Modal.Header>
        <Modal.Content image>
          <Form onSubmit={this.handleEditPost} style={{ margin: '0 auto'}}>
            <Form.TextArea
              name="body"
              value={body}
              placeholder="What is the news?"
              onChange={ev => this.setState({ body: ev.target.value })}
            />
            {imageLink && (
              <div className={styles.imageWrapper}>
                <Image className={styles.image} src={imageLink} alt="post" />
              </div>
            )}
            <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
              <Icon name="image" />
              Attach image
              <input name="image" type="file" onChange={this.handleUploadFile} hidden />
            </Button>
            <Button floated="right" color="red" onClick={this.onCancel}>Cancel</Button>
            <Button floated="right" color="green" type="submit">Ok</Button>
          </Form>
        </Modal.Content>
      </Modal >
    );
  }
}

const mapStateToProps = state => state.postEditor;

const mapDispatchToProps = {
  ...actions,
  updatePost
}

export default connect(mapStateToProps, mapDispatchToProps)(PostEditor);

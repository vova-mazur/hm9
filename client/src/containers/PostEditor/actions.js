import {
  SET_CURRENT_POST,
  DROP_CURRENT_POST,
  SHOW_PAGE,
  HIDE_PAGE
} from './actionTypes';

export const setCurrentPost = post => ({
  type: SET_CURRENT_POST,
  post
});

export const dropCurrentPost = () => ({
  type: DROP_CURRENT_POST
});

export const showPage = () => ({
  type: SHOW_PAGE
})

export const hidePage = () => ({
  type: HIDE_PAGE
})